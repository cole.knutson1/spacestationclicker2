(
  function() {
    var self = this;
    self.buttonCreate = new coelcanth();
    self.buttonCreate.createDPSGenerator("Laser", 3, 4);
    var secondsElapsed = 0;
    var clickRate = 1;
    var money = 50;
    var currentYear = 2112;
    var yearIterationFramerate = 500;
    var asteroidIteratorMax = 100;
    var asteroidFinderIterator = 0;
    var asteroidFinderIterationFramerate = 60;
    var moneyIterationFramerate = 5;
    var moneyIterator = 0,
      yearIterator = 0;
    var isSearchingForAsteroid = false;
    var asteroidsFound = 0;
    var secondFrameCount = 0;
    var quarterSecondFrameCount = 0;
    var autoClickValue = 0;
    var enemyHealthMax = 100;
    var enemyHealthRemaining = 100;
    var enemyKillTimer = 10;
    var isBoss = false;
    var dpsOverrideForDebugging = null;
    var enemyDeathCount = 0;
    var clickerDpsUpgradeCount = 0;
    var clickerDps = 3;
    var maxLevelAchieved = 0;
    var prestigeMultiplier = 0;
    var nextPrestigeValue;

    //DPS
    var dps = 0;
    var laserCount = 0;
    var cruiserCount = 0;
    var laserDPS = 2;
    var cruiserDps = 10;

    //COSTS
    var initial_ClickUpgradeCost = 10;
    var initial_ClickerDpsUpgradeCost = 10;
    var initial_autoClickUpgradeCost = 50;
    var initial_laserUpgradeCost = 25;
    var cost_AsteroidSearch = 500;
    var initial_cruiserUpgradeCost = 300;

    var clickUpgradeCost = initial_ClickUpgradeCost;
    var autoClickerUpgradeCost = initial_autoClickUpgradeCost;
    var laserUpgradeCost = initial_laserUpgradeCost;
    var cruiserUpgradeCost = initial_cruiserUpgradeCost;
    var clickerDpsUpgradeCost = initial_ClickerDpsUpgradeCost;

    var placeholderBossPrice = 0;
    var placeholderLootRewardPrice = 0;

    //Rewards
    var enemyLootRewardInitial = 10;
    var enemyLootReward = enemyLootRewardInitial;
    adjustDps();
    //Setup GUI with COSTS
    //SearchForAsteroids
    document.getElementById("search-for-asteroid-button").disabled = true;
    document.getElementById("search-for-asteroid-button").textContent =
      "Search for mineable asteroid (" + cost_AsteroidSearch + ")";

    document.getElementById("clicker-upgrade").disabled = true;
    document.getElementById("clicker-upgrade").textContent =
      "Clicker Upgrade (" + clickUpgradeCost + ")";

    document.getElementById("auto-clicker-upgrade").disabled = true;
    document.getElementById("auto-clicker-upgrade").textContent =
      "Auto-clicker Upgrade (" + autoClickerUpgradeCost + ")";

    document.getElementById("laser-upgrade").disabled = true;
    document.getElementById("laser-upgrade").textContent =
      "Laser Upgrade (" + laserUpgradeCost + ")";

    document.getElementById("cruiser-upgrade").disabled = true;
    document.getElementById("cruiser-upgrade").textContent =
      "Cruiser Upgrade (" + cruiserUpgradeCost + ")";

    document.getElementById("clicker-dps-upgrade").disabled = true;
    document.getElementById("clicker-dps-upgrade").textContent =
      "Clicker DPS (" + clickerDpsUpgradeCost + ")";

    document.getElementById("clicker-dps-upgrade").textContent =
      "Clicker DPS (" + clickerDpsUpgradeCost + ")";

    self.searchForAsteroid = function() {
      money -= cost_AsteroidSearch;
      isSearchingForAsteroid = true;
      document.getElementById("search-for-asteroid-button").disabled = true;
      document.getElementById("search-for-asteroid-button").textContent =
        "Searching for mineable asteroid...";
    };

    self.upgradeClicker = function() {
      money -= clickUpgradeCost;
      if (clickRate == 1) {
        clickRate = 4
      } else {
        clickRate = Math.ceil(clickRate * 1.5);
      }
      clickUpgradeCost = clickRate * initial_ClickUpgradeCost * ((1.07) ^ 10);
      clickerDps++;
      document.getElementById("clicker-upgrade").textContent =
        "Clicker Upgrade (" + clickUpgradeCost + ")";
    };

    self.upgradeAutoClicker = function() {
      money -= autoClickerUpgradeCost;
      debugger;
      autoClickValue += Math.ceil((autoClickValue + 1) * 1.25);
      autoClickerUpgradeCost = autoClickValue * 7 * ((1.07) ^ 10);
      document.getElementById("auto-clicker-upgrade").textContent =
        "Auto-clicker Upgrade (" + autoClickerUpgradeCost + ")";
    };

    self.upgradeClickerDPS = function() {
      money -= clickerDpsUpgradeCost;
      clickerDpsUpgradeCount++;
      // isBoss = true;
      clickerDps++;
      clickerDpsUpgradeCost = Math.ceil(Math.pow(clickerDpsUpgradeCount, 3));
      adjustDps();
    }


    self.upgradeLaser = function() {
      money -= laserUpgradeCost;
      laserCount++;
      // isBoss = true;
      laserUpgradeCost = Math.ceil(Math.pow(laserUpgradeCost, 1.25));
      // Math.ceil(laserCount * (initial_laserUpgradeCost / 2) * ((1.07) ^ 10));
      document.getElementById("laser-upgrade").textContent =
        "Laser Upgrade (" + laserUpgradeCost + ")";
      adjustDps();
    }

    self.upgradeCruiser = function() {
      money -= cruiserUpgradeCost;
      cruiserCount++;
      // isBoss = true;
      cruiserUpgradeCost = Math.ceil((cruiserUpgradeCost) * ((1.07) ^ 5));
      document.getElementById("cruiser-upgrade").textContent =
        "Cruiser Upgrade (" + cruiserUpgradeCost + ")";
      adjustDps();
    }

    function adjustDps() {
      if (dpsOverrideForDebugging == null) {
        dps = (cruiserCount * cruiserDps) + (laserDPS * laserCount);
      } else {
        dps = dpsOverrideForDebugging;
      }
    }

    function attack() {
      enemyHealthRemaining -= clickerDps;
      // isBoss = true;
    }

    function executeSecondlyEvents() {
      if (isBoss) {
        enemyKillTimer -= 1;
        document.getElementById("time-left-to-kill").textContent = enemyKillTimer;
      }

      if (isBoss && enemyKillTimer <= 0) {
        enemyKillTimer = 10;
        enemyHealthRemaining = enemyHealthMax;
        // if (dps == 0)
        //   isBoss = false;
      }

      if (!isBoss) {
        document.getElementById("time-left-to-kill").textContent = "";
      }
    }


    function executeQuarterSecondlyEvents() {
      money += autoClickValue;
      enemyHealthRemaining -= dps;
    }

    function draw() {
      // Draw the state of the world
      // document.getElementById("asteroid-count").textContent = asteroidsFound;
      document.getElementById("money-text").textContent = money;
      document.getElementById("dps-text").textContent = "DPS: " + dps * 4;
      document.getElementById("click-value").textContent = "Click: " + clickRate;
      document.getElementById("auto-click-value-text").textContent = "Autoclick: " + autoClickValue * 4;
      document.getElementById("enemy-death-count").textContent = "Enemy Death Count: " + enemyDeathCount;
      document.getElementById("enemy-health").textContent = enemyHealthRemaining + "/" + enemyHealthMax;
      document.getElementById("enemy-loot-reward").textContent = enemyLootReward;
      document.getElementById("clicker-dps-count").textContent = "x" + clickerDpsUpgradeCount;
      document.getElementById("laser-dps-count").textContent = "x" + laserCount;
      document.getElementById("cruiser-dps-count").textContent = "x" + cruiserCount;
      document.getElementById("enemy-health-bar").style.width = (enemyHealthRemaining / enemyHealthMax * 100).toString() + "%";
    }

    function update(progress) {
      quarterSecondFrameCount++;
      secondFrameCount++;

      //Second Elapsed
      if (secondFrameCount >= 60) {
        secondsElapsed += 1;
        secondFrameCount = 0;
        document.getElementById("frame-counter").textContent = secondsToHms(secondsElapsed);
        executeSecondlyEvents();
      }

      if (quarterSecondFrameCount >= 15) {
        executeQuarterSecondlyEvents();
        quarterSecondFrameCount = 0;
      }

      if (isSearchingForAsteroid) {
        asteroidFinderIterator += 1;
      }

      if (asteroidFinderIterator > asteroidIteratorMax) {
        asteroidFinderIterator = 0;
        asteroidsFound += 1;
        isSearchingForAsteroid = false;
        document.getElementById("search-for-asteroid-button").textContent =
          "Search for mineable asteroid (" + cost_AsteroidSearch + ")";
      }

      document.getElementById("search-for-asteroid-button").disabled = (money >= cost_AsteroidSearch &&
        !isSearchingForAsteroid) ? false : true;
      document.getElementById("clicker-upgrade").disabled = money >= clickUpgradeCost ? false : true;
      document.getElementById("auto-clicker-upgrade").disabled = money >= autoClickerUpgradeCost ? false : true;
      document.getElementById("laser-upgrade").disabled = money >= laserUpgradeCost ? false : true;
      document.getElementById("cruiser-upgrade").disabled = money >= cruiserUpgradeCost ? false : true;
      document.getElementById("clicker-dps-upgrade").disabled = money >= clickerDpsUpgradeCost ? false : true;
    }

    function loop(timestamp) {
      var progress = timestamp - lastRender
      checkEnemyHealth();
      update(progress)
      draw()

      lastRender = timestamp
      window.requestAnimationFrame(loop)
    }

    $('body').click(function() {
      if (event.target.tagName == "BUTTON") {
        return;
      }
      if (event.target.id == "cock-pit-window") {
        attack();
        return;
      }
      money += clickRate;
    });

    $(window).keyup(function(e) {
      if (e.keyCode == 32) {
        // user has pressed space
        money += clickRate;
      }
      if (e.keyCode == 13) {
        attack();
      }
    });

    function checkEnemyHealth() {
      if (enemyHealthRemaining <= 0) {
        enemyKillTimer = 10;
        money += enemyLootReward;
        enemyLootReward = Math.ceil(1.35 * enemyLootReward);
        enemyDeathCount++;
        handlePrestige();
        createNewEnemy();
      }
    }

    function handlePrestige() {
      if (enemyDeathCount > maxLevelAchieved) {
        nextPrestigeValue = (enemyDeathCount - maxLevelAchieved) * .01
        document.getElementById("prestige-wrapper").style.display = "BLOCK";
        document.getElementById("prestige-percentage").textContent = (nextPrestigeValue + 1).toString();
      }
    }

    self.prestige = function() {
      maxLevelAchieved = enemyDeathCount;
      reset();
    }

    function reset() {
      prestigeMultiplier = prestigeMultiplier + nextPrestigeValue;
      enemyDeathCount = 0;
      laserCount = 0;
      cruiserCount = 0;
      clickRate = 1;
      clickerDpsUpgradeCount = 0;
      autoClickValue = 0;
      autoClickerUpgradeCost = initial_autoClickUpgradeCost;
      clickerDpsUpgradeCost = initial_ClickerDpsUpgradeCost;
      cruiserUpgradeCost = initial_cruiserUpgradeCost;
    }

    function createNewEnemy() {
      enemyHealthMax = Math.ceil(50 + Math.pow(enemyDeathCount, 3));
      if (enemyDeathCount % 10 == 0) {
        isBoss = true;
        placeholderBossPrice = enemyHealthMax;
        enemyHealthMax = Math.ceil(enemyHealthMax * 1.25);
      } else {
        //If we go from boss to no boss, want less health than we had for boss
        if (isBoss == true) {
          enemyHealthMax = placeholderBossPrice;
        }
        isBoss = false;
      }
      enemyHealthRemaining = enemyHealthMax;
    }

    function secondsToHms(d) {
      d = Number(d);
      var h = Math.floor(d / 3600);
      var m = Math.floor(d % 3600 / 60);
      var s = Math.floor(d % 3600 % 60);

      var hDisplay = h > 0 ? h : "";
      var mDisplay = m > 0 ? m : "";
      var sDisplay = s > 0 ? s : "";
      return hDisplay.toString().padStart(2, '0') + ":" +
        mDisplay.toString().padStart(2, '0') + ":" +
        sDisplay.toString().padStart(2, '0');
    }

    var lastRender = 0;
    window.requestAnimationFrame(loop)
  })();